<?php

/**
 * ----------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 *
 * @Author      : Alexandre Caillot
 * @WebSite     : https://www.shiros.fr
 *
 * @File        : di.php
 * @Created_at  : 09/11/2024
 * @Update_at   : 25/11/2024
 * ----------------------------------------------------------------
 */

/**
 * Dependency injector configuration.
 */
return [
    /**
     * DI Modules.
     * Dependency injector modules are plugins allowing the construction of objects.
     *
     * The module definition looks like :
     *  - MyClass::class => MyClassModule::class
     */
    'Modules' => []
];
