<?php

/**
 * --------------------------------------------------------------------------
 * @Copyright   : License MIT 2016-2025
 * @WebSite     : https://www.shiros.fr
 *
 * @Author      : Alexandre Caillot
 *
 * @File        : AppModule.php
 * @Created_at  : 09/11/2024
 * @Update_at   : 05/01/2025
 * --------------------------------------------------------------------------
 */

namespace App;

use Luna\Component\Container\Exception\ContainerException;
use Luna\Component\Manager\PathManager;
use Luna\Component\Module\AbstractModule;
use Luna\Config\LunaConfig;

class AppModule extends AbstractModule
{
    # --------------------------------
    # Core methods

    /**
     * @inheritDoc
     *
     * @throws ContainerException
     */
    public function boot(): void
    {
        // ----------------
        // Vars

        // Get config
        $config      = $this->getContainer()->getConfig();
        $environment = $this->getContainer()->getEnvironment();

        // Paths
        $applicationDir = $this->getRootDir();
        $configDir      = PathManager::sanitize("{$applicationDir}/config");
        $configEnvDir   = PathManager::sanitize("{$configDir}/{$environment->getEnv()}");

        // ----------------
        // Process

        /**
         * Register application configuration.
         * - Main configuration
         * - Configuration for defined environment
         */
        $config
            ->addFiles(files: $this->getConfigMapping())
            ->load(path: $configDir)
            ->load(path: $configEnvDir)
        ;
    }

    # --------------------------------
    # Getters

    /**
     * Returns your own config mapping.
     * The mapping is used to load configuration at start of your application.
     *
     * @see LunaConfig::BASIC_FILES
     *
     * @return array
     */
    protected function getConfigMapping(): array
    {
        return [];
    }
}
