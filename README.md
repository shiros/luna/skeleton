# Luna PHP Framework Skeleton

## Table of Contents

[[_TOC_]]

## Information

This is the Luna skeleton.  
You'll be able to create projects based on the Luna framework, enabling you to build **YOUR** own application.

This project is in ***Php 8.2***.  
You can access to the wiki [here](https://gitlab.com/shiros/luna/skeleton/-/wikis/home).

### License

This project is licensed under the MIT license.  
See the [LICENSE](./LICENSE) file for more details.

### Dependencies

In this project, we'll use some external libs. You can see them below.

- [Luna Framework](https://gitlab.com/shiros/luna/luna): You can access to the MIT
  license [here](https://gitlab.com/shiros/luna/luna/-/blob/master/LICENSE)

## Installation

***Coming soon***

## Authors

- [Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)

## Contributors

- [Maxime Mazet (ElBidouilleur)](https://gitlab.com/ElBidouilleur)
